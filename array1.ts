const names: String[] = [];
names.push("Arthit");
names.push("Muangsiri");

console.log(names[0]);
console.log(names[1]);
console.log(names.length);

for (let i = 0; i < names.length; i++) {
    console.log(names[i]);

}

for (const name in names) {
    console.log(names[name]);
}

names.forEach(function (name) {
    console.log(name);
});

