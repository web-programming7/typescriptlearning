function printStatusCode(code: string | number) {
    if (typeof code != 'string') {
        console.log(`My status code is ${code} ${typeof code}`); //alt+96
    }else{
        console.log(`My status code is ${code.toUpperCase()} ${typeof code}`); //alt+96
    }
   
}
printStatusCode(404);
printStatusCode("arthit");