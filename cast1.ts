let x: unknown = "hello";
console.log((x as string).length);
console.log((<string>x).length);
console.log((("1" as unknown) as number).toFixed(2))